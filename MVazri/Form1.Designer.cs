﻿namespace MVazri
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.openfile = new System.Windows.Forms.Button();
            this.cameraSnap = new System.Windows.Forms.Button();
            this.videoLive = new System.Windows.Forms.Button();
            this.connectCamera = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.hSmartWindowControl1 = new HalconDotNet.HSmartWindowControl();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SetNCC_ROI = new System.Windows.Forms.Button();
            this.detectNCC = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // openfile
            // 
            this.openfile.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("openfile.BackgroundImage")));
            this.openfile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.openfile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.openfile.Location = new System.Drawing.Point(12, 410);
            this.openfile.Name = "openfile";
            this.openfile.Size = new System.Drawing.Size(48, 48);
            this.openfile.TabIndex = 1;
            this.openfile.UseVisualStyleBackColor = true;
            this.openfile.Click += new System.EventHandler(this.openfile_Click);
            // 
            // cameraSnap
            // 
            this.cameraSnap.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cameraSnap.BackgroundImage")));
            this.cameraSnap.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cameraSnap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cameraSnap.Location = new System.Drawing.Point(120, 410);
            this.cameraSnap.Name = "cameraSnap";
            this.cameraSnap.Size = new System.Drawing.Size(48, 48);
            this.cameraSnap.TabIndex = 2;
            this.cameraSnap.UseVisualStyleBackColor = true;
            // 
            // videoLive
            // 
            this.videoLive.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("videoLive.BackgroundImage")));
            this.videoLive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.videoLive.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.videoLive.Location = new System.Drawing.Point(174, 410);
            this.videoLive.Name = "videoLive";
            this.videoLive.Size = new System.Drawing.Size(48, 48);
            this.videoLive.TabIndex = 3;
            this.videoLive.UseVisualStyleBackColor = true;
            // 
            // connectCamera
            // 
            this.connectCamera.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("connectCamera.BackgroundImage")));
            this.connectCamera.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.connectCamera.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.connectCamera.Location = new System.Drawing.Point(66, 410);
            this.connectCamera.Name = "connectCamera";
            this.connectCamera.Size = new System.Drawing.Size(48, 48);
            this.connectCamera.TabIndex = 4;
            this.connectCamera.UseVisualStyleBackColor = true;
            this.connectCamera.Click += new System.EventHandler(this.connectCamera_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(480, 51);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(209, 199);
            this.richTextBox1.TabIndex = 5;
            this.richTextBox1.Text = "";
            // 
            // hSmartWindowControl1
            // 
            this.hSmartWindowControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.hSmartWindowControl1.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.hSmartWindowControl1.HDoubleClickToFitContent = true;
            this.hSmartWindowControl1.HDrawingObjectsModifier = HalconDotNet.HSmartWindowControl.DrawingObjectsModifier.None;
            this.hSmartWindowControl1.HImagePart = new System.Drawing.Rectangle(0, 0, 640, 480);
            this.hSmartWindowControl1.HKeepAspectRatio = true;
            this.hSmartWindowControl1.HMoveContent = true;
            this.hSmartWindowControl1.HZoomContent = HalconDotNet.HSmartWindowControl.ZoomContent.WheelForwardZoomsIn;
            this.hSmartWindowControl1.Location = new System.Drawing.Point(12, 27);
            this.hSmartWindowControl1.Margin = new System.Windows.Forms.Padding(0);
            this.hSmartWindowControl1.Name = "hSmartWindowControl1";
            this.hSmartWindowControl1.Size = new System.Drawing.Size(440, 368);
            this.hSmartWindowControl1.TabIndex = 6;
            this.hSmartWindowControl1.WindowSize = new System.Drawing.Size(440, 368);
            this.hSmartWindowControl1.Load += new System.EventHandler(this.hSmartWindowControl1_Load);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // SetNCC_ROI
            // 
            this.SetNCC_ROI.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("SetNCC_ROI.BackgroundImage")));
            this.SetNCC_ROI.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SetNCC_ROI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SetNCC_ROI.Location = new System.Drawing.Point(259, 410);
            this.SetNCC_ROI.Name = "SetNCC_ROI";
            this.SetNCC_ROI.Size = new System.Drawing.Size(48, 48);
            this.SetNCC_ROI.TabIndex = 7;
            this.SetNCC_ROI.UseVisualStyleBackColor = true;
            this.SetNCC_ROI.Click += new System.EventHandler(this.SetNCC_ROI_Click);
            // 
            // detectNCC
            // 
            this.detectNCC.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("detectNCC.BackgroundImage")));
            this.detectNCC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.detectNCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.detectNCC.Location = new System.Drawing.Point(313, 410);
            this.detectNCC.Name = "detectNCC";
            this.detectNCC.Size = new System.Drawing.Size(48, 48);
            this.detectNCC.TabIndex = 8;
            this.detectNCC.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 465);
            this.Controls.Add(this.detectNCC);
            this.Controls.Add(this.SetNCC_ROI);
            this.Controls.Add(this.hSmartWindowControl1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.connectCamera);
            this.Controls.Add(this.videoLive);
            this.Controls.Add(this.cameraSnap);
            this.Controls.Add(this.openfile);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button openfile;
        private System.Windows.Forms.Button cameraSnap;
        private System.Windows.Forms.Button videoLive;
        private System.Windows.Forms.Button connectCamera;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private HalconDotNet.HSmartWindowControl hSmartWindowControl1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button SetNCC_ROI;
        private System.Windows.Forms.Button detectNCC;
    }
}

